#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding("utf-8")
from flask import Flask, render_template, request, json, redirect, session, url_for, flash, jsonify
# from flaskext.mysql import MySQL
import psycopg2
import sys
import json
import psycopg2.extras
#from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)



app.debug = True

app.config['SECRET_KEY'] = 'secretOfTheRunes'
# app.secret_key = 'why would I tell you my secret key?'
app.config['DATABASE_NAME'] = 'test' #nazwa bazy danych
app.config['DATABASE_HOST'] = 'localhost' #host bazy danych
app.config['DATABASE_USER'] = 'postgres' #użytkownik bazy danych
app.config['DATABASE_PASS'] = 'postgres' #hasło do bazy danych



#conn = psycopg2.connect(database=app.config['DATABASE_NAME'],user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)
# MySQL configurations
# app.config['MYSQL_DATABASE_USER'] = 'root'
# app.config['MYSQL_DATABASE_PASSWORD'] = 'lucatel'
# app.config['MYSQL_DATABASE_DB'] = 'lucatel-geoportal'
# app.config['MYSQL_DATABASE_HOST'] = 'localhost'

# mysql.init_app(app)

@app.route("/")
def main():
    return render_template('index.html')


@app.route("/postGIS")
def postGIS():
    conn = psycopg2.connect(database='test',user='postgres',host='localhost',password='postgres',port=5432)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("""
        SELECT row_to_json(fc)
          FROM ( SELECT 'FeatureCollection' AS type, array_to_json(array_agg(f)) AS features
                   FROM ( SELECT 'Feature' AS type,
                                 ST_AsGeoJSON(geom)::JSON AS geometry
                          FROM slupy) AS f )  AS fc;
        """)
    # cursor.execute("""
    #     SELECT row_to_json(fc)
    #       FROM ( SELECT 'FeatureCollection' AS type, array_to_json(array_agg(f)) AS features
    #                FROM ( SELECT 'Feature' AS type,
    #                              ST_AsGeoJSON(lg.geom)::JSON AS geometry,
    #                              row_to_json((SELECT l FROM (SELECT name) AS l)) AS properties
    #                       FROM slupy AS lg ) AS f )  AS fc
    #     """)
    # cursor.execute(u"SELECT row_to_json(f) As feature FROM (SELECT 'FeatureCollection' As type, ST_AsGeoJSON(l.geom)::json As geometry, row_to_json(l) As properties FROM test As l) As f;")
    # conn.commit()
    data = cursor.fetchall()
    # data = str(data)
    # cursor.close()
    # conn.close()
    # jsonify(result=data[0][0])

    # str(json.dumps(list(data.dictresult())))

    return jsonify(result=data[0][0])


# @app.route('/showSignin')
# def showSignin():
#     return render_template('signin.html')



@app.route('/someurl/putpoint/', methods=['GET', 'POST'])
def putpoint():
    # conn.close()
    conn = psycopg2.connect(database='test',user='postgres',host='localhost',password='postgres',port=5432)
    # conn = psycopg2.connect(database='test',user='postgres',host='localhost',password='postgres',port=5432)
    cursor = conn.cursor()
    # NB: massively insecure accepting any random input from PUT!
    # Should use the params arg in execute() rather than format str
    cursor.execute("INSERT INTO test(geom) VALUES (ST_GeomFromText('POINT(%s %s)', 4326));" % (request.form['lng'], request.form['lat']))
    # cursor.close()
    conn.commit()
    cursor.close()
    # conn.close()
    conn.close()
    # flash("To jest wiadomość typu flash")
    return "Udało się wprowadzić punkt"







if __name__ == '__main__':
    app.run(host='0.0.0.0')

