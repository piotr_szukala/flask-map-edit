var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.5, -0.09]).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup();



////////////////////////////////////////////////////////////////////////////////////////////////////

map.on('click', function(e) {
  $.ajax('/someurl/putpoint/', {
    type: 'POST',
    data: {
      lat: e.latlng.lat,
      lng: e.latlng.lng
    },
    success: function(response) {
      // document.write(response);
      $('#message').addClass('success').fadeIn(600).animate({opacity: 1.0}, 1500).fadeOut(600);
      $('#message p').html(response);
    },
    error: function(response) {
      $('#message').addClass('error').fadeIn(600).animate({opacity: 1.0}, 1500).fadeOut(600);
      $('#message p').html(response);
    }
  });
});

  // $.ajax({
  //   url: "http://gis.lucatel.pl:8080/geoserver/pawonkow/wfs?format_options=callback:getJson1",
  //   data: {
  //     service: 'WFS',
  //     version: '1.1.0',
  //     request: 'GetFeature',
  //     typename: 'feature:stacje',
  //     maxFeatures: 200,
  //     outputFormat: 'text/javascript',
  //     srsName: "EPSG:4326",
  //     geometryName: "geom",
  //     async: false,
  //   },
  //   method: "get",
  //   dataType: 'jsonp',
  //   jsonpCallback:'getJson1',
  //   // jsonp:'format_options',
  //   success: handleJson
  // });
var geojsonlayer;



  $.ajax({
    url: postGISUrl,
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    mimeType: 'application/json',
    success: function (response) {
    //console.log(data);
    geojsonlayer = L.geoJson(response, {
      // pointToLayer: function (feature, latlng) {
      //     return L.circleMarker(latlng);
      // }
      // style: myStyle,
      // onEachFeature: onEachFeature
    }).addTo(map);
    // $(document).trigger('hideLoader');
    // map.fitBounds(geojsonlayer.getBounds());
  },
    error: function (data, status, er) {
      console.log(er);
    }
  });

    function handleJson(data) {
    //console.log(data);
    geojsonlayer = L.geoJson(data, {
      pointToLayer: function (feature, latlng) {
          return L.circleMarker(latlng);
      }
      // style: myStyle,
      // onEachFeature: onEachFeature
    }).addTo(map);
    // $(document).trigger('hideLoader');
    // map.fitBounds(geojsonlayer.getBounds());
  };
