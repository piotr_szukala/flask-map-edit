$(function() {
    $('#btnSignIn').click(function() {

        $.ajax({
            url: '/validateLogin', //wysyła formularz na inna strone
            data: $('form').serialize(), //form to element html a metoda serialize() zmienia dane z firmularza w tekst do wysylki
            type: 'POST',
            success: function(response) {
                document.write(response); //Umozliwia renderowanie szablonu we flasku
                //console.log(response); // Jesli zostanie tylko to to renderowanie szablonu bedzie tylko w konsoli
                //window.location.href = '/';
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});
